

public abstract class Producto{
	abstract boolean EstaDisponible(int cant);
	abstract int CalcularTarifa();
}
