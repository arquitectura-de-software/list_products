
public class Servicio extends Producto{

	String nombre;
	//number of services just for test
	int precio;
	int cant_servicio;
	
	Servicio(int precio, int cant_servicio){
		this.cant_servicio = cant_servicio;
		this.precio = precio;
	}
	@Override
	boolean EstaDisponible(int number_services_required) {
		if (number_services_required>100)
			return false;
		else
			return true;
		
	}

	@Override
	int CalcularTarifa() {
		if (EstaDisponible(cant_servicio))
			return cant_servicio * precio;
		else
			return 0;
	}

}
