
public class Articulo extends Producto{
	String nombre;
	int saldo;
	int precio;
	int cant_articulos;
	
	Articulo(int precio, int cant_articulos,int saldo)
	{
		this.cant_articulos = cant_articulos;
		this.precio = precio;
		this.saldo = saldo;
	}
	@Override
	boolean EstaDisponible(int cant) {
		if (cant > 20)
			return false;
		else
			return true;
	}
	
	@Override
	int CalcularTarifa() {
		
		if (EstaDisponible(cant_articulos))
			return cant_articulos * precio - cant_articulos * saldo;
		else 
			return 0;
		
	}
}
