import java.util.ArrayList;

public class Ventas {
	ArrayList<Producto> Productos = new ArrayList<Producto>();
	Ventas ()
	{
	}
	public void Add_Product (Producto Nuevo_Producto) {
		Productos.add(Nuevo_Producto);
	}
	
	public int CalcularTarifa() {
		int resultado = 0;
		for (Producto producto : Productos) {
			resultado += producto.CalcularTarifa();
		}
		return resultado;
	}
}
