import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;


class ProductoTest {
	//Articulos
	//Orden {Precio} {Numero Requerido}
	@Test
	void Deberia_devolver_9_para_precio_5_cantidad_3_y_saldo_2() {
		Producto prod1 = new Articulo(5,3,2);
		assertEquals(9, prod1.CalcularTarifa()); 
	}

	@Test
	void Deberia_devolver_10_para_precio_4_y_cantidad_5_y_saldo_2() {
		Producto prod1 = new Articulo(4,5,2);
		assertEquals(10, prod1.CalcularTarifa()); 
	}

	@Test
	void Deberia_devolver_0_para_precio_4_y_cantidad_500_y_saldo_2() {
		Producto prod1 = new Articulo(4,500,2);
		assertEquals(0, prod1.CalcularTarifa()); 
	}
	//Servicios
	@Test
	void Deberia_devolver_500_para_precio_10_y_cantidad_50() {
		Producto prod1 = new Servicio(10,50);
		assertEquals(500, prod1.CalcularTarifa()); 
	}
	
	@Test
	void Deberia_devolver_200_para_precio_10_y_cantidad_20() {
		Producto prod1 = new Servicio(10,20);
		assertEquals(200, prod1.CalcularTarifa()); 
	}
	
	@Test
	void Deberia_devolver_60_para_precio_10_y_cantidad_6() {
		Producto prod1 = new Servicio(10,6);
		assertEquals(60, prod1.CalcularTarifa()); 
	}
	
	@Test
	void Deberia_devolver_0_para_precio_10_y_cantidad_600() {
		Producto prod1 = new Servicio(10,600);
		assertEquals(0, prod1.CalcularTarifa()); 
	}
	//Vemtas	

	@Test
	void Deberia_devolver_30_total(){
		Ventas sales = new Ventas();
		Producto prod1 = new Articulo(10,6,5);
		sales.Add_Product(prod1);
		assertEquals(30, sales.CalcularTarifa());
	}
	
	@Test
	void Deberia_devolver_90_total(){
		Ventas sales = new Ventas();
		Producto prod1 = new Articulo(10,6,5);
		Producto prod2 = new Servicio(10,6);
		sales.Add_Product(prod1);
		sales.Add_Product(prod2);
		assertEquals(90, sales.CalcularTarifa());
	}
}
